import XCTest

import ZafeplaceSDKTests

var tests = [XCTestCaseEntry]()
tests += ZafeplaceSDKTests.allTests()
XCTMain(tests)
